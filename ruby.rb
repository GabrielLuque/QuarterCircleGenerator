require 'ruby2d'
require 'csv'
puts "Hello, world!"

tick = 0

set width: 300, height: 300 
$quart = Circle.new(
	x:0, y:300,
	radius:300,
	sectors: 32,
	color: 'red',
	z: 1
)
$innerQuart = Circle.new(
	x:0, y:300,
	radius:290,
	sectors: 32,
	color: 'white',
	z: 2
)


def pairGen(num)
	list = []
	num.times {
		x = rand().to_f
		y = rand().to_f
		list << [x,y] 



}	
return list
end

def isInsideCircle(pair)
	#puts (pair[0]*300)
	#puts (pair[1]*300)

	d = Math.sqrt(((pair[0]*300)- $quart.x )**2+((pair[1]*300)- $quart.y)**2)
	#puts d
	if d<$quart.radius
		return TRUE
	else
		return FALSE 
	end

end
def generator(genN,genT)
iter = 1
ls = []
puts genT
genT.times{
		k=0		
		ls = pairGen(genN)
		ls.each do |points|

		if(isInsideCircle(points))
			k += 1
		end 
		end
puts "#{iter}K=#{k} K/N=#{k.to_f/genN.to_f}"
CSV.open("results.csv","ab") do |csv|
	csv << [k,k.to_f/genN.to_f]
end
CSV.open("test.csv","wb") do |csv|
	csv << ["X","Y","Inside Circle?"]
	ls.each do |points| 
	csv << points.push(isInsideCircle(points))
	end
	csv << ["Amount Inside the Circle?", k, "From San Juan with Love"]
	end
iter +=1
}
return ls
end
$genN = 0
$genT = 0

if ARGV.size()>0
#	puts ARGV[1]
	$genN = ARGV[0].to_i
	if ARGV.size() == 2
		$genT = ARGV[1].to_i
	end
else
	puts "No arguments running with default values."
	$genN = 100
	$genT = 1
end

ls = generator($genN, $genT)
$t = 0

update do
	#puts $t
	if $t <$genN
		#puts isInsideCircle(ls[$t])
		color= "red"
		if isInsideCircle(ls[$t])
	 		color = "green"
		end	
		Circle.new(
		x:ls[$t][0]*300, y:ls[$t][1]*300 , 
		radius: 2,
		color: color,
		z: 10
		)
		
		$t += 1
	else
	puts "Done!"
	sleep(5)
	exit
	end 
end
show

